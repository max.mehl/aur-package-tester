# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2021 Gergely Imreh <gergely@imreh.net>
# SPDX-FileCopyrightText: 2021 Max Mehl <https://mehl.mx>

# Forked from https://github.com/imrehg/archlinux-makepkg-docker/

FROM archlinux as base

RUN pacman -Syuq --noconfirm git base-devel sudo

RUN echo "Defaults         lecture = never" > /etc/sudoers.d/privacy \
 && echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel \
 && useradd -m -G wheel -s /bin/bash builder

USER builder
WORKDIR /home/builder

RUN git clone https://aur.archlinux.org/yay.git \
 && cd yay \
 && makepkg -s --noconfirm

######
# Runtime container
######
FROM archlinux

RUN pacman -Syuq --noconfirm git base-devel sudo namcap python-srcinfo \
 && rm -rf /var/cache/pacman/pkg/*

RUN echo "Defaults         lecture = never" > /etc/sudoers.d/privacy \
 && echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel \
 && useradd -m -G wheel -s /bin/bash builder

USER builder
WORKDIR /home/builder

COPY --from=base /home/builder/yay/*.pkg.tar.* /home/builder/pkg/

RUN sudo pacman -U --noconfirm /home/builder/pkg/*.pkg.tar.*

COPY install_deps.py /home/builder/

WORKDIR /home/builder/aur/
